import React from 'react'

export const AllComics = () => {
    const { data, isLoading, error } = useFetch('https://gateway.marvel.com:443/v1/public/comics?limit=4&ts=epoch&apikey=23403bb598a7e1b4ab84d380b25afe5b&hash=fa778ca48636f0025fc708f3227e4ca3')
  console.log(data);
    //const [isDataLoading, setIsDataLoading] = useState(false)
  console.log(data);
  
  const resultsData = data?.data?.results
  console.log(resultsData);
  return (
    <div>
        <ul>
            {resultsData.map((result, index) => (
                <li key={index}>{result.name}</li>
            ))}
        </ul>
        
    </div>
  )
}
