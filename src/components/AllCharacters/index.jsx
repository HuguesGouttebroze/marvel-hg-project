import React from 'react'
import { Card, CardMedia, CardContent, Typography, Button, CardActions } from '@mui/material';

export default function MediaCard({props}) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        sx={{ height: 140 }}
        image={`.jpg`}
        title="green iguana"
      />
      <img src={props.r.thumbnail.path} alt={props.r.name} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.r.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Lizards are a widespread group of squamate reptiles, with over 6,000
          species, ranging across all continents except Antarctica
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Share</Button>
        <Button size="small">Learn More</Button>
      </CardActions>
    </Card>
  );
}
