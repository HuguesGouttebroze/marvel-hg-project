import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import { useEffect } from 'react';
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import styled, { keyframes } from 'styled-components'
import axios from 'axios'
import { useFetch } from './hooks/useFetch';
import { responsiveFontSizes } from '@mui/material';
import MediaCard from './components/AllCharacters';
 
const rotate = keyframes`
    from {
        transform: rotate(0deg);
    }
 
    to {
    transform: rotate(360deg);
    }
`
 
export const Loader = styled.div`
    padding: 10px;
    border: 6px solid black;
    border-bottom-color: transparent;
    border-radius: 22px;
    animation: ${rotate} 1s infinite linear;
    height: 0;
    width: 0;
`


function App() {
  //const [data, setData] = useState([])
  const [count, setCount] = useState(0)
  const [isDataLoading, setIsDataLoading] = useState(false)
  const { data, isLoading, error } = useFetch('https://gateway.marvel.com:443/v1/public/characters?limit=100&ts=epoch&apikey=23403bb598a7e1b4ab84d380b25afe5b&hash=fa778ca48636f0025fc708f3227e4ca3')
  console.log(data);
  
  const resultsData = data?.data?.results
  console.log(resultsData);
  // const _testFakeDatas = https://jsonplaceholder.typicode.com/users
  // const _apiBase = '';

/*   useEffect(() => {
    setIsDataLoading(true)
    axios.get('https://gateway.marvel.com:443/v1/public/characters?limit=4&ts=epoch&apikey=23403bb598a7e1b4ab84d380b25afe5b&hash=fa778ca48636f0025fc708f3227e4ca3')
    .then((d) => setData(d.data))
    //.then((data) => console.log(data.data))
    setIsDataLoading(false)
    
  }, []) */

  return isLoading ? (
    <>
      <Loader />
    </>
    ) : (
    <div className="App">
      <div>
        <header className='header'>
          <h1>Marvels</h1>
          <h3>...from official API...</h3>
          <navbar>
            <ul>
              <li><a href="#">All characters</a></li>
              <li><a href="#">All comics</a></li>
              <li><a href="#"></a></li>
              <li><a href="#">By character </a></li>
              <li><a href="#">By comic</a></li>
              <li><a href="#"></a></li>
            </ul>
          </navbar>
        </header>
        <img src="/marvel_logo.png" className="logo" alt="Vite logo" />
  {/*       {resultsData.map((r, index) => (
          <MediaCard key={index} r={r} />
        ))} */}
        <div>
        {resultsData.map((r, index) => (
          <div key={index} className='card'>
            <strong>{r.name}</strong>
            <br />
            <img src={`${r.thumbnail.path}.${r.thumbnail.extension}`} alt={r.name}  height="350" />
            <p>{`...into ${r.comics.available} comics`}</p>
{/*             <ul>
              {r.comics.items.map((comic, index) => (
                <li key={index}>{comic.name}</li>
              ))}
            </ul> */}
          </div>
        ))}
        </div>
        <a href="https://vitejs.dev" target="_blank">
          
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
